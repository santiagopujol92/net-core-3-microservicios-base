﻿using Domain.Entities;
using System;
using System.Collections.Generic;
//using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders ;


namespace Infraestructure.Persistance.PostgresSQL.EntityConfig
{
    public class ContactoConfiguration : IEntityTypeConfiguration<Contacto>
    {
        public void Configure(EntityTypeBuilder<Contacto> builder)
           {
            // Mapeo a la tabla Personas (en plural)            
            builder.ToTable("Contactos");
            //builder.Property(p => p.Persona.Id).HasColumnName("Id");
            //builder.HasOne(c => c.Persona).WithMany().HasForeignKey(nameof(Persona));
            //builder.Property("Id");

            // set Id como primaryKey
            //builder.HasKey(p => p.Id);

            // El campo Id sera IdPersona en Base de datos
            //builder.Property(p => p.Id).HasField("IdPersona");

            // setear como no permitir null y tamañoo maximo de 150
            //builder.Property(p => p.Nombres ).IsRequired().HasMaxLength(150);

            // setear como no permitir null y tamañoo maximo de 150
            //builder.Property(p => p.Apellidos).IsRequired().HasMaxLength(150);

            // setear como no permitir null
            //builder.Property(p => p.FechaNacimiento ).IsRequired();

            //Property(p => p.Nacionalidad).IsRequired();



        }
    }
}
