﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders ;


namespace Infraestructure.Persistance.PostgresSQL.EntityConfig
{
    public class PersonaConfiguration : IEntityTypeConfiguration<Persona>
    {
        public void Configure(EntityTypeBuilder<Persona> builder)
           {
            // Mapeo a la tabla Personas (en plural)            
            builder.ToTable("Personas");            
            // set Id como primaryKey
            builder.HasKey(p => p.Id);

            // El campo Id sera IdPersona en Base de datos
            //builder.Property(p => p.Id).HasField("IdPersona");

            // setear como no permitir null y tamañoo maximo de 150
            builder.Property(p => p.Nombres).IsRequired().HasMaxLength(150);
            // setear como no permitir null y tamañoo maximo de 150
            builder.Property(p => p.Apellidos).IsRequired().HasMaxLength(150);

            // setear como no permitir null
            builder.Property(p => p.FechaNacimiento).IsRequired();

            builder.HasIndex(p => p.Dni).IsUnique();

            builder.HasOne<Nacionalidad>(p =>p.Nacionalidad).WithMany().HasForeignKey(n => n.NacionalidadId);
            builder.HasOne<EstadoCivil>(p=> p.EstadoCivil).WithMany().HasForeignKey(n => n.EstadoCivilId);
            builder.HasIndex("DomicilioId").IsUnique();
        }
    }
}
