﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Persistance.PostgresSQL.Migrations
{
    public partial class nahuel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Personas_DomicilioId",
                table: "Personas");

            migrationBuilder.CreateIndex(
                name: "IX_Personas_DomicilioId",
                table: "Personas",
                column: "DomicilioId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Personas_DomicilioId",
                table: "Personas");

            migrationBuilder.CreateIndex(
                name: "IX_Personas_DomicilioId",
                table: "Personas",
                column: "DomicilioId");
        }
    }
}
