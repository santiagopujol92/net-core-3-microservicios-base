﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Infraestructure.Persistance.PostgresSQL.Migrations
{
    public partial class nahuel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Personas_Domicilio_DomicilioId",
                table: "Personas");

            migrationBuilder.DropForeignKey(
                name: "FK_TareasLaborales_Empleados_EmpleadoId",
                table: "TareasLaborales");

            migrationBuilder.DropTable(
                name: "Archivos");

            migrationBuilder.DropTable(
                name: "TareasLaborales_DocumentacionObligatoria");

            migrationBuilder.DropTable(
                name: "Documentaciones");

            migrationBuilder.DropTable(
                name: "TipoDocumentacion");

            migrationBuilder.DropTable(
                name: "Empleados");

            migrationBuilder.DropTable(
                name: "CategoriasLaborales");

            migrationBuilder.DropTable(
                name: "ConveniosSindicales");

            migrationBuilder.DropTable(
                name: "TareasLaborales");

            migrationBuilder.AlterColumn<Guid>(
                name: "DomicilioId",
                table: "Personas",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_Personas_Domicilio_DomicilioId",
                table: "Personas",
                column: "DomicilioId",
                principalTable: "Domicilio",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Personas_Domicilio_DomicilioId",
                table: "Personas");

            migrationBuilder.AlterColumn<Guid>(
                name: "DomicilioId",
                table: "Personas",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "CategoriasLaborales",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoriasLaborales", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConveniosSindicales",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConveniosSindicales", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDocumentacion",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDocumentacion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Empleados",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Activo = table.Column<bool>(nullable: false),
                    CategoriaLaboralId = table.Column<string>(nullable: true),
                    ConvenioSindicalId = table.Column<string>(nullable: true),
                    FechaIngreso = table.Column<DateTime>(nullable: false),
                    Legajo = table.Column<string>(nullable: true),
                    TareaLaboralId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empleados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Empleados_CategoriasLaborales_CategoriaLaboralId",
                        column: x => x.CategoriaLaboralId,
                        principalTable: "CategoriasLaborales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Empleados_ConveniosSindicales_ConvenioSindicalId",
                        column: x => x.ConvenioSindicalId,
                        principalTable: "ConveniosSindicales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Empleados_Personas_Id",
                        column: x => x.Id,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Documentaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Descripcion = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<Guid>(nullable: true),
                    FechaVencimiento = table.Column<DateTime>(nullable: false),
                    PersonaId = table.Column<Guid>(nullable: true),
                    TipoDocumentacionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documentaciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documentaciones_Empleados_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documentaciones_Personas_PersonaId",
                        column: x => x.PersonaId,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documentaciones_TipoDocumentacion_TipoDocumentacionId",
                        column: x => x.TipoDocumentacionId,
                        principalTable: "TipoDocumentacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TareasLaborales",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TareasLaborales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TareasLaborales_Empleados_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Archivos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Base64 = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    DocumentacionId = table.Column<int>(nullable: true),
                    ExtensionArchivo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Archivos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Archivos_Documentaciones_DocumentacionId",
                        column: x => x.DocumentacionId,
                        principalTable: "Documentaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TareasLaborales_DocumentacionObligatoria",
                columns: table => new
                {
                    TareaLaboralId = table.Column<string>(nullable: false),
                    TiposDocumentacionId = table.Column<string>(nullable: false),
                    TipoDocumentacionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TareasLaborales_DocumentacionObligatoria", x => new { x.TareaLaboralId, x.TiposDocumentacionId });
                    table.ForeignKey(
                        name: "FK_TareasLaborales_DocumentacionObligatoria_TareasLaborales_Ta~",
                        column: x => x.TareaLaboralId,
                        principalTable: "TareasLaborales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TareasLaborales_DocumentacionObligatoria_TipoDocumentacion_~",
                        column: x => x.TipoDocumentacionId,
                        principalTable: "TipoDocumentacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Archivos_DocumentacionId",
                table: "Archivos",
                column: "DocumentacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentaciones_EmpleadoId",
                table: "Documentaciones",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentaciones_PersonaId",
                table: "Documentaciones",
                column: "PersonaId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentaciones_TipoDocumentacionId",
                table: "Documentaciones",
                column: "TipoDocumentacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleados_CategoriaLaboralId",
                table: "Empleados",
                column: "CategoriaLaboralId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleados_ConvenioSindicalId",
                table: "Empleados",
                column: "ConvenioSindicalId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleados_TareaLaboralId",
                table: "Empleados",
                column: "TareaLaboralId");

            migrationBuilder.CreateIndex(
                name: "IX_TareasLaborales_EmpleadoId",
                table: "TareasLaborales",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_TareasLaborales_DocumentacionObligatoria_TipoDocumentacionId",
                table: "TareasLaborales_DocumentacionObligatoria",
                column: "TipoDocumentacionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Personas_Domicilio_DomicilioId",
                table: "Personas",
                column: "DomicilioId",
                principalTable: "Domicilio",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Empleados_TareasLaborales_TareaLaboralId",
                table: "Empleados",
                column: "TareaLaboralId",
                principalTable: "TareasLaborales",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
