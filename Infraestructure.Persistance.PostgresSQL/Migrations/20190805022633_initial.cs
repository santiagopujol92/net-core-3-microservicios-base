﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Infraestructure.Persistance.PostgresSQL.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategoriasLaborales",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoriasLaborales", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConveniosSindicales",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConveniosSindicales", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Empresas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RazonSocial = table.Column<string>(nullable: true),
                    CUIT = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empresas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EstadoCivil",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstadoCivil", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Nacionalidades",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nacionalidades", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoContacto",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoContacto", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDocumentacion",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDocumentacion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Personas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nombres = table.Column<string>(maxLength: 150, nullable: false),
                    Apellidos = table.Column<string>(maxLength: 150, nullable: false),
                    FechaNacimiento = table.Column<DateTime>(nullable: false),
                    LugarNacimiento = table.Column<string>(nullable: true),
                    Dni = table.Column<string>(nullable: true),
                    Cuil = table.Column<string>(nullable: true),
                    NacionalidadId = table.Column<string>(nullable: true),
                    EstadoCivilId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Personas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Personas_EstadoCivil_EstadoCivilId",
                        column: x => x.EstadoCivilId,
                        principalTable: "EstadoCivil",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Personas_Nacionalidades_NacionalidadId",
                        column: x => x.NacionalidadId,
                        principalTable: "Nacionalidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contacto",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TipoContactoId = table.Column<string>(nullable: true),
                    EmpresaId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contacto_Empresas_EmpresaId",
                        column: x => x.EmpresaId,
                        principalTable: "Empresas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contacto_Personas_Id",
                        column: x => x.Id,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contacto_TipoContacto_TipoContactoId",
                        column: x => x.TipoContactoId,
                        principalTable: "TipoContacto",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Empleados",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FechaIngreso = table.Column<DateTime>(nullable: false),
                    Activo = table.Column<bool>(nullable: false),
                    Legajo = table.Column<string>(nullable: true),
                    CategoriaLaboralId = table.Column<string>(nullable: true),
                    TareaLaboralId = table.Column<string>(nullable: true),
                    ConvenioSindicalId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empleados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Empleados_CategoriasLaborales_CategoriaLaboralId",
                        column: x => x.CategoriaLaboralId,
                        principalTable: "CategoriasLaborales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Empleados_ConveniosSindicales_ConvenioSindicalId",
                        column: x => x.ConvenioSindicalId,
                        principalTable: "ConveniosSindicales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Empleados_Personas_Id",
                        column: x => x.Id,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Documentaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Descripcion = table.Column<string>(nullable: true),
                    FechaVencimiento = table.Column<DateTime>(nullable: false),
                    TipoDocumentacionId = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<Guid>(nullable: true),
                    PersonaId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documentaciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documentaciones_Empleados_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documentaciones_Personas_PersonaId",
                        column: x => x.PersonaId,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documentaciones_TipoDocumentacion_TipoDocumentacionId",
                        column: x => x.TipoDocumentacionId,
                        principalTable: "TipoDocumentacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TareasLaborales",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TareasLaborales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TareasLaborales_Empleados_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Archivos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Descripcion = table.Column<string>(nullable: true),
                    Base64 = table.Column<string>(nullable: true),
                    ExtensionArchivo = table.Column<int>(nullable: false),
                    DocumentacionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Archivos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Archivos_Documentaciones_DocumentacionId",
                        column: x => x.DocumentacionId,
                        principalTable: "Documentaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TareasLaborales_DocumentacionObligatoria",
                columns: table => new
                {
                    TareaLaboralId = table.Column<string>(nullable: false),
                    TiposDocumentacionId = table.Column<string>(nullable: false),
                    TipoDocumentacionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TareasLaborales_DocumentacionObligatoria", x => new { x.TareaLaboralId, x.TiposDocumentacionId });
                    table.ForeignKey(
                        name: "FK_TareasLaborales_DocumentacionObligatoria_TareasLaborales_Ta~",
                        column: x => x.TareaLaboralId,
                        principalTable: "TareasLaborales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TareasLaborales_DocumentacionObligatoria_TipoDocumentacion_~",
                        column: x => x.TipoDocumentacionId,
                        principalTable: "TipoDocumentacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Archivos_DocumentacionId",
                table: "Archivos",
                column: "DocumentacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Contacto_EmpresaId",
                table: "Contacto",
                column: "EmpresaId");

            migrationBuilder.CreateIndex(
                name: "IX_Contacto_TipoContactoId",
                table: "Contacto",
                column: "TipoContactoId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentaciones_EmpleadoId",
                table: "Documentaciones",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentaciones_PersonaId",
                table: "Documentaciones",
                column: "PersonaId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentaciones_TipoDocumentacionId",
                table: "Documentaciones",
                column: "TipoDocumentacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleados_CategoriaLaboralId",
                table: "Empleados",
                column: "CategoriaLaboralId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleados_ConvenioSindicalId",
                table: "Empleados",
                column: "ConvenioSindicalId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleados_TareaLaboralId",
                table: "Empleados",
                column: "TareaLaboralId");

            migrationBuilder.CreateIndex(
                name: "IX_Personas_Dni",
                table: "Personas",
                column: "Dni",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Personas_EstadoCivilId",
                table: "Personas",
                column: "EstadoCivilId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Personas_NacionalidadId",
                table: "Personas",
                column: "NacionalidadId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TareasLaborales_EmpleadoId",
                table: "TareasLaborales",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_TareasLaborales_DocumentacionObligatoria_TipoDocumentacionId",
                table: "TareasLaborales_DocumentacionObligatoria",
                column: "TipoDocumentacionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Empleados_TareasLaborales_TareaLaboralId",
                table: "Empleados",
                column: "TareaLaboralId",
                principalTable: "TareasLaborales",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Empleados_Personas_Id",
                table: "Empleados");

            migrationBuilder.DropForeignKey(
                name: "FK_TareasLaborales_Empleados_EmpleadoId",
                table: "TareasLaborales");

            migrationBuilder.DropTable(
                name: "Archivos");

            migrationBuilder.DropTable(
                name: "Contacto");

            migrationBuilder.DropTable(
                name: "TareasLaborales_DocumentacionObligatoria");

            migrationBuilder.DropTable(
                name: "Documentaciones");

            migrationBuilder.DropTable(
                name: "Empresas");

            migrationBuilder.DropTable(
                name: "TipoContacto");

            migrationBuilder.DropTable(
                name: "TipoDocumentacion");

            migrationBuilder.DropTable(
                name: "Personas");

            migrationBuilder.DropTable(
                name: "EstadoCivil");

            migrationBuilder.DropTable(
                name: "Nacionalidades");

            migrationBuilder.DropTable(
                name: "Empleados");

            migrationBuilder.DropTable(
                name: "CategoriasLaborales");

            migrationBuilder.DropTable(
                name: "ConveniosSindicales");

            migrationBuilder.DropTable(
                name: "TareasLaborales");
        }
    }
}
