﻿using Domain.Entities;
using Infraestructure.Persistance.PostgresSQL.EntityConfig;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;


namespace Infraestructure.Persistance.PostgresSQL
{
    public class CenturyContext : DbContext
    {
        public CenturyContext() : base()
        {
            //"Server=defti-dev.westus2.cloudapp.azure.com;Database=prueba;User Id=sa;Password = Nahueldipaolo2018;Timeout=5 "
            // "RicardoModeloDB debe estar configurado en el config de la aplicacion.
            // Ej...
            // <add name="RicardoModeloDB" connectionString="Data Source=LUISFERNANDO-NO;Initial Catalog=ProjetoModeloDB;Integrated Security=True;MultipleActiveResultSets=True" providerName="System.Data.SqlClient" />
        }

        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Persona> Personas { get; set; }
        public DbSet<Nacionalidad> Nacionalidades { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=defti-dev.westus2.cloudapp.azure.com; Database = prueba; User Id = sa; Password = Nahueldipaolo2018; Timeout = 5 ");         
            //"Server=.\;Database=EFCoreWebDemo;Trusted_Connection=True;MultipleActiveResultSets=true");

            // Conexion Nahuel
            //optionsBuilder.UseNpgsql(@"Server=127.0.0.1;Port=5433;Database=Century-MCP-Santiago;User Id=postgres;Password = 1234;CommandTimeout=10;;Timeout=10 ");

            // Conexion Santi
            optionsBuilder.UseNpgsql(@"Server=127.0.0.1;Port=5432;Database=Century-MCP;User Id=postgres;Password = root;CommandTimeout=10;;Timeout=10 ");

            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PersonaConfiguration());
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("FechaRegistro") != null))
            {
                if(entry.State == EntityState.Added)
                {
                    entry.Property("FechaRegistro").CurrentValue = DateTime.Now;
                }
                if(entry.State == EntityState.Modified)
                {
                    entry.Property("FechaRegistro").IsModified = false;
                }
            }

            return base.SaveChanges();
        }
    }
}
