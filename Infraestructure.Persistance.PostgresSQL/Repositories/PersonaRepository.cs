﻿using Domain.Entities;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Persistance.PostgresSQL.Repositories
{
    public class PersonaRepository : BaseRepository<Persona>, IPersonaRepository
    {

        public Persona GetById(Guid id)
        {
            return base.GetById(id);
        }

        public Nacionalidad GetNacionalidad(String id)
        {
            return db.Set<Nacionalidad>().Find(id);
        }

        public EstadoCivil GetEstadoCivil(String id)
        {
            return db.Set<EstadoCivil>().Find(id);
        }

        public void Add(Domicilio domicilio)
        {
            db.Set<Domicilio>().Add(domicilio);            
        }

        public void Update(Domicilio domicilio)
        {
            db.Set<Domicilio>().Update(domicilio);
        }

        public Persona GetByDni(string dni)
        {
            return db.Set<Persona>().Where(p => p.Dni == dni.Trim()).FirstOrDefault<Persona>();
        }

        public Persona GetByCuit(string cuit)
        {
            return db.Set<Persona>().Where(p => p.Cuil == cuit.Trim()).FirstOrDefault<Persona>();
        }

        public Persona GetByEmail(string email)
        {
            return db.Set<Persona>().Where(p => p.Email == email.Trim()).FirstOrDefault<Persona>();
        }

    }
}
