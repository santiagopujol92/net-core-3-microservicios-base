﻿using System;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Core.Querying;
using System.Data.SqlClient;

namespace Infraestructure.Querying.SqlServerClient
{
    public sealed class SqlServerTableDataGateway : TableDataGateway
    {
        private readonly string connectionString;

        public SqlServerTableDataGateway(string connectionString)
        {
            this.connectionString = connectionString;
        }

        protected override DbCommand CreateCommand(string sql, DbConnection connection)
        {
            return new  SqlCommand(sql, (SqlConnection)connection);
        }

        protected override DbConnection CreateDatabaseConnection()
        {
            return new SqlConnection(this.connectionString);
        }

        protected override DbParameter CreateParameter()
        {
            return new SqlParameter();
        }

        protected override WhereClauseBuilder<TTableObject> CreateWhereClauseBuilder<TTableObject>()
        {
            return new SqlServerWhereClauseBuilder<TTableObject>();
        }
    }
}
