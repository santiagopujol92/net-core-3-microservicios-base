﻿
using System.Collections.Generic;

namespace Domain.Shared
{
    public class ServiceResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }

    }
}
