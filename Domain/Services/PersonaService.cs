﻿using Domain.Core;
using Domain.Core.Services;
using Domain.Entities;
using Domain.Entities.Persona_Raiz_Agregada;
using Domain.Interfaces.Repositories;
using Domain.Specifications;
using System;
using System.Collections.Generic;

namespace Domain.Interfaces.Services
{
    public class PersonaService: DomainService, IPersonaService
    {
        public Persona CrearPersona(IPersonaRepository personaRepository, Persona persona)
        {
            var personaValidator = new PersonaValidator();
            if (personaValidator.IsValid(persona))
            {
                personaRepository.Add(persona);
                personaRepository.Commit();
                personaRepository.LoadReference(persona, p => p.Nacionalidad);
                personaRepository.LoadReference(persona, p => p.EstadoCivil );
                return persona;
            }
            else
            {               
                throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "No se puede crear la persona. No se cumplió una validación de datos.", "Persona", personaValidator.BrokenRules(persona)));
            }
        }

        public IEnumerable<Persona> ObtenerPersonas(IPersonaRepository personaRepository)
        {
            var personaResult = personaRepository.GetAll();

            if (personaResult != null)
            {
                return personaResult;
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se pudo obtener las personas", ""));
            }
        }

        public Persona ObtenerPersonaById(IPersonaRepository personaRepository, Guid id)
        {
            var personaResult = personaRepository.GetById(id);

            if (personaResult is null) {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se pudo encontrar la persona con el id " + id, "Persona"));
            }
            else
            {
                return personaResult;                
            }
        }

        public Persona ModificarPersonaById(IPersonaRepository personaRepository, Persona persona)
        {
            var personaValidator = new PersonaValidator();
            if (personaValidator.IsValid(persona))
            {
                personaRepository.Update(persona);
                personaRepository.Commit();
                return persona;
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "No se puede modificar la persona", "Persona", personaValidator.BrokenRules(persona)));
            };
        }

        public bool EliminarPersonaById(IPersonaRepository personaRepository, Guid id)
        {
            var personaResult = personaRepository.GetById(id);

            if (personaResult != null)
            {
                personaRepository.Remove(personaResult);
                personaRepository.Commit();
                return true;
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se pudo encontrar la persona con el id " + id, "Persona"));
            }
        }

        public Domicilio CrearDomicilioPersona(IPersonaRepository personaRepository, Domicilio domicilio, Guid idPersona)
        {
            var domicilioValidator = new DomicilioValidator();
            if (domicilioValidator.IsValid(domicilio))
            {
                var persona = personaRepository.GetById(idPersona);
                if (persona is null)
                {
                    throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se encuentra la persona ingresada para asignarle el domicilio.", "Persona", null));
                }
                else
                {
                    personaRepository.Add(domicilio);
                    persona.setDomicilio(domicilio);
                    personaRepository.Update(persona);
                    personaRepository.Commit();
                    return domicilio;
                }
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "No se puede crear el domicilio. No se cumplió una validación de datos.", "Domicilio", domicilioValidator.BrokenRules(domicilio)));
            }
        }

        public Domicilio ModificarDomicilioPersona(IPersonaRepository personaRepository, Domicilio domicilio, Guid idPersona, Guid idDomicilio)
        {
            var domicilioValidator = new DomicilioValidator();
            if (domicilioValidator.IsValid(domicilio))
            {
                var persona = personaRepository.GetById(idPersona);
                if (persona is null)
                {
                    throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se encuentra la persona ingresada para asignarle el domicilio.", "Persona", null));
                }
                else
                {
                    if (persona.Domicilio.Id == idDomicilio)
                    {
                        personaRepository.Update(domicilio);
                        personaRepository.Commit();
                        return domicilio;
                    } else
                    {
                        throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "No se puede crear el domicilio. El domicilio ingresado no coincide con el de la persona", "Domicilio"));
                    }
                }
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "No se puede crear el domicilio. No se cumplió una validación de datos.", "Domicilio", domicilioValidator.BrokenRules(domicilio)));
            }
        }

        public Persona ObtenerPersonaByDni(IPersonaRepository personaRepository, string dni)
        {
            var personaDniSpecification = new PersonaSpecification.DniSpecification();

            if (personaDniSpecification.IsValidDni(dni)) {
                var personaResult = personaRepository.GetByDni(dni);
                
                if (personaResult is null)
                {
                    throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se pudo encontrar la persona con el dni " + dni, "Persona"));
                }
                else
                {
                    return personaResult;
                }
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "El Dni debe contener 9 dígitos y ser sólo números", "Dni", null));
            };
        }


        public Persona ObtenerPersonaByCuit(IPersonaRepository personaRepository, string cuit)
        {
            var personaCuilSpecification = new PersonaSpecification.CuilSpecification();
            if (personaCuilSpecification.ValidaCuit(cuit))
            {
                var personaResult = personaRepository.GetByCuit(cuit);

                if (personaResult is null)
                {
                    throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se pudo encontrar la persona con el cuit " + cuit, "Persona"));
                }
                else
                {
                    return personaResult;
                }
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "El cuit es inválido", "Cuit", null));
            };
        }

        public Persona ObtenerPersonaByEmail(IPersonaRepository personaRepository, string email)
        {
            var personaEmailSpecification = new PersonaSpecification.EmailSpecification();
            if (personaEmailSpecification.IsValidEmail(email))
            {
                var personaResult = personaRepository.GetByEmail(email);

                if (personaResult is null)
                {
                    throw new CenturyException(new CenturyError(CenturyError.TipoError.NoEncontrado, "No se pudo encontrar la persona con el email " + email, "Persona"));
                }
                else
                {
                    return personaResult;
                }
            }
            else
            {
                throw new CenturyException(new CenturyError(CenturyError.TipoError.ErrorValidacion, "El email es inválido", "Cuit", null));
            };

        }

    }
}
