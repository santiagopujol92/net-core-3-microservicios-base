﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Core;

namespace Domain.Entities
{
    public class Contacto : Entity<Guid>, IAggregateRoot
    {
        // Esta configuracion no respeta el principio de independencia de la persistencia que plantea DDD
        // Se codifico de esta manera porque no pudimos resolverlo con Fluent API de EF Core
        [ForeignKey(nameof(Persona))]
        public override Guid Id { get; protected set; }
        public virtual Persona Persona { get; private set; }

        public virtual TipoContacto TipoContacto { get; private set; }

        public Contacto() { }

        public Contacto(Guid id, string nombres, string apellidos, DateTime fechanacimiento, string lugarnacimiento, string dni, string cuil, TipoContacto tipoContacto, string nacionalidadId, string estadoCivilId)
        {
            this.Persona = new Persona(id, nombres, apellidos, fechanacimiento, lugarnacimiento, dni, cuil, nacionalidadId, estadoCivilId);
            this.TipoContacto = tipoContacto;
        }

    }
}
