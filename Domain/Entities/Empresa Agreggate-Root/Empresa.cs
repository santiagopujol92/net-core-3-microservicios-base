﻿using System;
using System.Collections.Generic;
using Domain.Core;

namespace Domain.Entities
{
    public class Empresa : Entity<Guid>, IAggregateRoot
    {

        public string RazonSocial { get; private set; }
        public string CUIT { get; private set; }

        private readonly List<Contacto> _Contactos;

        public virtual IReadOnlyCollection<Contacto> Contactos => _Contactos.AsReadOnly();

        public Empresa() {}

        public Empresa(Guid id, string razonSocial, string cuit)
        { 
            this.Id = id;
            this.RazonSocial  = razonSocial;
            this.CUIT = cuit;
        }
               
        public void CambiarRazonSocial(string razonSocial)
        {
            this.RazonSocial = razonSocial;
        }

        public void CambiarCUIT(string cuit)
        {
            this.CUIT = cuit;
        }

    }
}
