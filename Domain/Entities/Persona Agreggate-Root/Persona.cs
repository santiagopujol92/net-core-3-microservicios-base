﻿using System;
using Domain.Core;

namespace Domain.Entities
{
    public class Persona : Entity<Guid>, IAggregateRoot
    {
        public string Nombres { get; private set; }
        public string Apellidos { get; private set; }
        public DateTime FechaNacimiento { get; private set; }
        public string LugarNacimiento { get; private set; }
        public string Dni { get; private set; }
        public string Cuil { get; private set; }
        public string Email { get; private set; }
        public string TelefonoMovil { get; private set; }

        public string NacionalidadId { get; private set; }
        public virtual Nacionalidad Nacionalidad { get; private set; }

        public string EstadoCivilId { get; private set; }
        public virtual EstadoCivil EstadoCivil { get; private set; }

        public virtual Domicilio Domicilio { get; private set; }

        public Persona(Guid id, string nombres, string apellidos, DateTime fechaNacimiento, string lugarNacimiento, string dni, string cuil, string nacionalidadId, string estadoCivilId)
        {
            this.Id = id;
            this.Nombres = nombres;
            this.Apellidos = apellidos;
            this.FechaNacimiento = fechaNacimiento;
            this.LugarNacimiento = lugarNacimiento;
            this.Dni = dni;
            this.Cuil = cuil;
            this.NacionalidadId = nacionalidadId;
            this.EstadoCivilId = estadoCivilId;
        }

        public void setId(Guid id)
        {
            this.Id = id;
        }

        public void setNombres(string nombres)
        {
            this.Nombres = nombres;
        }

        public void setApellidos(string apellidos)
        {
            this.Apellidos = apellidos;
        }

        public void setFechaNacimiento(DateTime fechaNacimiento)
        {
            this.FechaNacimiento = fechaNacimiento;
        }

        public void setLugarNacimiento(string lugarNacimiento)
        {
            this.LugarNacimiento = lugarNacimiento;
        }

        public void setDni(string dni)
        {
            this.Dni = dni;
        }

        public void setCuil(string cuil)
        {
            this.Cuil = cuil;
        }

        public void setEmail(string email)
        {
            this.Email = email;
        }

        public void setTelefonoMovil(string telefonoMovil)
        {
            this.TelefonoMovil = telefonoMovil;
        }

        public void setNacionalidad(string nacionalidadId)
        {
            this.NacionalidadId = nacionalidadId;
            this.Nacionalidad = null;
        }

        public void setEstadoCivil(string estadoCivilId)
        {
            this.EstadoCivilId = estadoCivilId;
            this.EstadoCivil = null;
        }

        public void setDomicilio(Domicilio domicilio)
        {
            this.Domicilio = domicilio;
        }

    }

}
