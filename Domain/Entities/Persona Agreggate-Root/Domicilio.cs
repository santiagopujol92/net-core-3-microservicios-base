﻿using Domain.Core;
using System;

namespace Domain.Entities
{
    public class Domicilio : Entity<Guid>
    {
        public string Calle { get; private set; }
        public string Altura { get; private set; }
        public string Barrio { get; private set; }
        public string Depto { get; private set; }
        public string Piso { get; private set; }
        public string Manzana { get; private set; }
        public string Lote { get; private set; }
        public string Casa { get; private set; }
        public string Localidad { get; private set; }
        public string CodPostal { get; private set; }
        public string Observaciones { get; private set; }

        public Domicilio(Guid id, string calle, string altura, string barrio, string depto, string piso, string manzana, string lote, string casa, string localidad, string codPostal, string observaciones)
        {
            this.Id = id;
            this.Calle = calle;
            this.Altura = altura;
            this.Barrio = barrio;
            this.Depto = depto;
            this.Piso = piso;
            this.Manzana = manzana;
            this.Lote = lote;
            this.Casa = casa;
            this.Localidad = localidad;
            this.CodPostal = codPostal;
            this.Observaciones = observaciones;
        }

        public void setCalle(string calle)
        {
            this.Calle = calle;
        }

        public void setAltura(string altura)
        {
            this.Altura = altura;
        }

        public void setBarrio(string barrio)
        {
            this.Barrio = barrio;
        }

        public void setDepto(string depto)
        {
            this.Depto = depto;
        }

        public void setPiso(string piso)
        {
            this.Piso = piso;
        }

        public void setManzana(string manzana)
        {
            this.Manzana = manzana;
        }

        public void setLote(string lote)
        {
            this.Lote = lote;
        }

        public void setCasa(string casa)
        {
            this.Casa = casa;
        }

        public void setLocalidad(string localidad)
        {
            this.Localidad = localidad;
        }

        public void setCodPostal(string codPostal)
        {
            this.CodPostal = codPostal;
        }

        public void setObservaciones(string observaciones)
        {
            this.Observaciones = observaciones;
        }

    }
}
