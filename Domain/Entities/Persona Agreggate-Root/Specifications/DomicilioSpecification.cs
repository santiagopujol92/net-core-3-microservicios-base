﻿using Domain.Core.Specifications;
using System;
using System.Linq.Expressions;

namespace Domain.Entities.Persona_Raiz_Agregada
{
    public class DomicilioSpecification
    {
        public sealed class CodPostalSpecification : Specification<Domicilio>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Domicilio, bool>> Expression => domicilio => !string.IsNullOrEmpty(domicilio.CodPostal);
            #endregion
        }

        public sealed class LocalidadSpecification : Specification<Domicilio>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Domicilio, bool>> Expression => domicilio => !string.IsNullOrEmpty(domicilio.Localidad);
            #endregion
        }

        public sealed class BarrioSpecification : Specification<Domicilio>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Domicilio, bool>> Expression => domicilio => !string.IsNullOrEmpty(domicilio.Barrio);
            #endregion
        }

        public sealed class CalleSpecification : Specification<Domicilio>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Domicilio, bool>> Expression => domicilio => !string.IsNullOrEmpty(domicilio.Calle);
            #endregion
        }

        public sealed class AlturaSpecification : Specification<Domicilio>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Domicilio, bool>> Expression => domicilio => !string.IsNullOrEmpty(domicilio.Altura);
            #endregion
        }
    }
}
