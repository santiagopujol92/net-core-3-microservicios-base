﻿using Domain.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace Domain.Entities.Persona_Raiz_Agregada
{
    public class PersonaSpecification
    {
        public sealed class NombresSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => !string.IsNullOrEmpty(persona.Nombres);
            #endregion
        }

        public sealed class ApellidosSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => !string.IsNullOrEmpty(persona.Apellidos);
            #endregion
        }

        public sealed class FechaNacimientoSpecification : Specification<Persona>
        {
            //private readonly string _department;

            #region Public Methods
            /// <summary>
            /// Obtiene la expresion LINQ que representa a la especificación.
            /// </summary>
            /// <returns>Expresión LINQ.</returns>
            public override Expression<Func<Persona, bool>> Expression => p => p.FechaNacimiento < System.DateTime.Now;
            #endregion

            //public PersonaSpecification(string depatrment)
            //{
            //    _department = depatrment;
            //}

        }

        public sealed class LugarNacimientoSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => !string.IsNullOrEmpty(persona.LugarNacimiento);
            #endregion
        }

        public sealed class DniSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => IsValidDni(persona.Dni);

            public bool IsValidDni(string dni)
            {
                dni = new System.Text.RegularExpressions.Regex(@"\D")
                    .Replace(dni, string.Empty);
                dni = dni.TrimStart('1');
                if (dni.Length == 9 && dni.All(char.IsDigit))
                {
                    return true;
                }

                return false;
            }
            #endregion
        }

        public sealed class CuilSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => ValidaCuit(persona.Cuil);

            /// <summary>
            /// Calcula el dígito verificador dado un CUIT completo o sin él.
            /// </summary>
            /// <param name="cuit">El CUIT como String sin guiones</param>
            /// <returns>El valor del dígito verificador calculado.</returns>
            public int CalcularDigitoCuit(string cuit)
            {
                int[] mult = new[] { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                char[] nums = cuit.ToCharArray();
                int total = 0;
                for (int i = 0; i<mult.Length; i++)
                {
                    total += int.Parse(nums[i].ToString()) * mult[i];
                }
                var resto = total % 11;
                return resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
            }

            /// <summary>
            /// Valida el CUIT ingresado.
            /// </summary>
            /// <param name="cuit" />Número de CUIT como string con o sin guiones
            /// <returns>True si el CUIT es válido y False si no.</returns>
            public bool ValidaCuit(string cuit)
             {
                 if (cuit == null)
                {
                    return false;
                }
               //Quito los guiones, el cuit resultante debe tener 11 caracteres.
                cuit = cuit.Replace("-", string.Empty);
                if (cuit.Length != 11)
                {
                    return false;
                }
                else
                {
                    int calculado = CalcularDigitoCuit(cuit);
                    int digito = int.Parse(cuit.Substring(10));
                    return calculado == digito;
                }
            }

            #endregion
        }

        public sealed class NacionalidadIdSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => !string.IsNullOrEmpty(persona.NacionalidadId);
            #endregion
        }

        public sealed class EstadoCivilIdSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => !string.IsNullOrEmpty(persona.EstadoCivilId);
            #endregion
        }

        public sealed class EmailSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => IsValidEmail(persona.Email);

            public bool IsValidEmail(string email)
            {
                string expression = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

                if (Regex.IsMatch(email, expression))
                {
                    if (Regex.Replace(email, expression, string.Empty).Length == 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            #endregion
        }

        public sealed class TelefonoMovilSpecification : Specification<Persona>
        {
            #region Public Methods
            /// <summary>
            /// Gets the LINQ expression which represents the current specification.
            /// </summary>
            /// <returns>The LINQ expression.</returns>
            public override Expression<Func<Persona, bool>> Expression => persona => IsValidPhoneNumber(persona.TelefonoMovil);

            public bool IsValidPhoneNumber(string value)
            {
                value = new System.Text.RegularExpressions.Regex(@"\D")
                    .Replace(value, string.Empty);
                value = value.TrimStart('1');
                if (value.Length >= 10 && value.Length <= 13 && value.All(char.IsDigit))
                {
                    return true;
                }
                    
                return false;
            }
            #endregion
        }
    }
}
