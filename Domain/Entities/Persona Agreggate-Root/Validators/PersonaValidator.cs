﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Core.Specifications;
using Domain.Entities;
using Domain.Core;
using System.Linq;
using Domain.Entities.Persona_Raiz_Agregada;

namespace Domain.Specifications
{
    public class PersonaValidator : IValidator<Persona>
    {
        private readonly IList<Specification<Persona>> Rules =
            new List<Specification<Persona>>
                {
                new PersonaSpecification.NombresSpecification(),
                new PersonaSpecification.ApellidosSpecification(),
                new PersonaSpecification.FechaNacimientoSpecification(),
                new PersonaSpecification.LugarNacimientoSpecification(),
                new PersonaSpecification.DniSpecification(),
                new PersonaSpecification.CuilSpecification(),
                new PersonaSpecification.NacionalidadIdSpecification(),
                new PersonaSpecification.EstadoCivilIdSpecification(),
                new PersonaSpecification.EmailSpecification(),
                new PersonaSpecification.TelefonoMovilSpecification(),
            };

        public bool IsValid(Persona persona)
        {
            return (BrokenRules(persona).Count() == 0);
        }

        public IEnumerable<CenturyError> BrokenRules(Persona persona)
        {
            return Rules.Where(rule => !rule.IsSatisfiedBy(persona))
                        .Select(rule => GetErrorsForBrokenRule(rule));
        }

        protected CenturyError GetErrorsForBrokenRule(Specification<Persona> specPersona)
        {
            switch (specPersona.GetType().Name)
            {
                case nameof(PersonaSpecification.NombresSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El nombre no puede estar vacío o nulo", "Nombres");
                case nameof(PersonaSpecification.ApellidosSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El apellido no puede estar vacío o nulo", "Apellidos");
                case nameof(PersonaSpecification.CuilSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El cuit es inválido", "Cuil");
                case nameof(PersonaSpecification.DniSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El Dni debe contener 9 dígitos y ser sólo números", "DNI");
                case nameof(PersonaSpecification.EstadoCivilIdSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El Estado Civil no puede estar vacío o nulo", "EstadoCivil");
                case nameof(PersonaSpecification.FechaNacimientoSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "La Fecha de nacimiento debe ser menor a la fecha de hoy", "FechaNacimiento");
                case nameof(PersonaSpecification.LugarNacimientoSpecification):
                    
                case nameof(PersonaSpecification.NacionalidadIdSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "La Nacionalidad no puede estar vacía o nulo", "Nacionalidad");
                case nameof(PersonaSpecification.EmailSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El email es inválido", "Email");
                case nameof(PersonaSpecification.TelefonoMovilSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El Teléfono Móvil debe contener entre 10 y 13 digitos y ser sólo números", "TelefonoMovil");
                default:
                    break;
            }
            return null;
        }
    }
}
