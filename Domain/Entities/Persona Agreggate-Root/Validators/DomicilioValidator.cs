﻿using System.Collections.Generic;
using Domain.Core.Specifications;
using Domain.Entities;
using Domain.Core;
using System.Linq;
using Domain.Entities.Persona_Raiz_Agregada;

namespace Domain.Specifications
{
    public class DomicilioValidator : IValidator<Domicilio>
    {
        private readonly IList<Specification<Domicilio>> Rules =
            new List<Specification<Domicilio>>
                {
                new DomicilioSpecification. AlturaSpecification(),
                new DomicilioSpecification.BarrioSpecification(),
                new DomicilioSpecification.CalleSpecification(),
                new DomicilioSpecification.CodPostalSpecification(),
                new DomicilioSpecification.LocalidadSpecification(),
            };

        public bool IsValid(Domicilio domicilio)
        {
            return (BrokenRules(domicilio).Count() == 0);
        }

        public IEnumerable<CenturyError> BrokenRules(Domicilio domicilio)
        {
            return Rules.Where(rule => !rule.IsSatisfiedBy(domicilio))
                        .Select(rule => GetErrorsForBrokenRule(rule));
        }

        protected CenturyError GetErrorsForBrokenRule(Specification<Domicilio> specDomicilio)
        {
            switch (specDomicilio.GetType().Name)
            {
                case nameof(DomicilioSpecification.AlturaSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "La altura no puede estar vacía o nulo", "Altura");
                case nameof(DomicilioSpecification.BarrioSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El barrio no puede estar vacío o nulo", "Barrio");
                case nameof(DomicilioSpecification.CalleSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "La calle no puede estar vacío o nulo", "Calle");
                case nameof(DomicilioSpecification.CodPostalSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "El Cod. Postal no puede estar vacío o nulo", "Cod. Postal");
                case nameof(DomicilioSpecification.LocalidadSpecification):
                    return new CenturyError(CenturyError.TipoError.ValorIncorrecto, "La Localidad no puede estar vacío o nulo", "Localidad");
                default:
                    break;
            }
            return null;
        }
    }
}
