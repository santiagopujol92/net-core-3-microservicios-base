﻿using Domain.Core.Services;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Domain.Shared;
using System;
using System.Collections.Generic;

namespace Domain.Interfaces.Services
{
    public interface IPersonaService : IDomainService
    {
        Persona CrearPersona(IPersonaRepository _personaRepository, Persona persona);
        IEnumerable<Persona> ObtenerPersonas(IPersonaRepository _personaRepository);
        Persona ObtenerPersonaById(IPersonaRepository _personaRepository, Guid id);
        Persona ModificarPersonaById(IPersonaRepository _personaRepository, Persona persona);
        bool EliminarPersonaById(IPersonaRepository _personaRepository, Guid id);
        Domicilio CrearDomicilioPersona(IPersonaRepository personaRepository, Domicilio domicilio, Guid idPersona);
        Domicilio ModificarDomicilioPersona(IPersonaRepository personaRepository, Domicilio domicilio, Guid idPersona, Guid idDomicilio);
        Persona ObtenerPersonaByDni(IPersonaRepository personaRepository, string dni);
        Persona ObtenerPersonaByCuit(IPersonaRepository personaRepository, string cuit);
        Persona ObtenerPersonaByEmail(IPersonaRepository personaRepository, string email);
        
    }
}
