﻿using Domain.Entities;
using Domain.Core.Repositories;
using System;

namespace Domain.Interfaces.Repositories
{
    public interface IPersonaRepository : IRepositoryBase<Persona>
    {
        Persona GetById(Guid id);
        Nacionalidad GetNacionalidad(String id);
        EstadoCivil GetEstadoCivil(String id);
        void Add(Domicilio domicilio);
        void Update(Domicilio domicilio);
        Persona GetByDni(string dni);
        Persona GetByCuit(string cuit);
        Persona GetByEmail(string email);
    }
}
