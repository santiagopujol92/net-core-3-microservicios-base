﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Domain.Entities;
using Application.ModelsDto.PersonaDto_Agreggate_Root;

namespace Application.ModelsDto
{
    public class AutoMapperMappingProfile : Profile
    {
        public AutoMapperMappingProfile()
        {
            CreateMap<PersonaDto, Persona>();
            CreateMap<Persona, PersonaDto>();
            CreateMap<DomicilioDto, Domicilio>();
            CreateMap<Domicilio, DomicilioDto>();

        }
    }
}
