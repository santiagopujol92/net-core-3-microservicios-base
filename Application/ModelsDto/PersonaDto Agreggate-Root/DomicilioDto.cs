﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ModelsDto.PersonaDto_Agreggate_Root
{
    public class DomicilioDto
    {
        public Guid Id { get; set; }
        public string Calle { get; set; }
        public string Altura { get; set; }
        public string Barrio { get; set; }
        public string Depto { get; set; }
        public string Piso { get; set; }
        public string Manzana { get; set; }
        public string Lote { get; set; }
        public string Casa { get; set; }
        public string Localidad { get; set; }
        public string CodPostal { get; set; }
        public string Observaciones { get; set; }
    }
}
