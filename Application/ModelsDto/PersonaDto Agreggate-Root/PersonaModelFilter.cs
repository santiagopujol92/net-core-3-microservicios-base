﻿
using System;

namespace Application.ModelsDto.PersonaDto_Agreggate_Root
{
    public class PersonaModelFilter
    {
        public Guid? Id { get; set; }
        public string Email { get; set; }
        public string Dni { get; set; }
        public string Cuit { get; set; }
    }
}
