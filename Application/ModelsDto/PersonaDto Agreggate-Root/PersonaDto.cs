﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ModelsDto.PersonaDto_Agreggate_Root
{
    public class PersonaDto
    {
        public Guid Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string LugarNacimiento { get; set; }
        public string Dni { get; set; }
        public string Cuil { get; set; }
        public string Email { get; set; }
        public string TelefonoMovil { get; set; }
        public string NacionalidadId { get; set; }
        public string EstadoCivilId { get; set; }
        public Guid? DomicilioId { get; set; }
    }
}
