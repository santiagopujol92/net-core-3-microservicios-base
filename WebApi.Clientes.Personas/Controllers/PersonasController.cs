﻿using Application.ModelsDto.PersonaDto_Agreggate_Root;
using AutoMapper;
using Domain.Core;
using Domain.Entities;
using Domain.Interfaces.Services;
using Infraestructure.Persistance.PostgresSQL.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using WebApi.Shared;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasController : ControllerBase
    {
        private PersonaRepository _personaRepository = new PersonaRepository();
        private PersonaService _personaService = new PersonaService();

        // Creacion de un campo para almacenar el objeto de Automapper
        private readonly IMapper _mapper;

        // Asigna el objeto de automapper en el constructor por dependency injection
        public PersonasController(IMapper mapper)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// Obtiene lista de personas o persona
        /// </summary>
        /// <returns>Retorna una lista de personas o personas</returns>
        /// <response code="200">Retorna una lista de personas o personas</response>
        /// <response code="404">Not Found</response>
        /// <response code="500">Error interno del servidor</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult Get([FromQuery]PersonaModelFilter filter)
        {
            if (string.IsNullOrEmpty(filter.Email)
            && string.IsNullOrEmpty(filter.Cuit)
            && string.IsNullOrEmpty(filter.Dni)
            && filter.Id == null)
            {
                var result = _personaService.ObtenerPersonas(_personaRepository);
                return Ok(result);
            }

            try
            {
                Persona result = null;

                if (!(string.IsNullOrEmpty(filter.Email)))
                    result = _personaService.ObtenerPersonaByEmail(_personaRepository, filter.Email);

                if (!(string.IsNullOrEmpty(filter.Dni)))
                    result = _personaService.ObtenerPersonaByDni(_personaRepository, filter.Dni);

                if (!(string.IsNullOrEmpty(filter.Cuit)))
                    result = _personaService.ObtenerPersonaByCuit(_personaRepository, filter.Cuit);

                if (filter.Id != null)
                    result = _personaService.ObtenerPersonaById(_personaRepository, (Guid)filter.Id);

                return Ok(new ApiOkResponse(result));
            }
            catch (CenturyException ex)
            {
                ObjectResult o = new ObjectResult(new ApiResponse(ex.CenturyError, ex.StackTrace));
                o.StatusCode = 500;
                return o;
            }

        }

        /// <summary>
        /// Crear Persona.
        /// </summary>
        /// <param name="request">PersonaDto Object.</param>
        /// <returns>Restorna id de persona/returns>
        /// <response code="201">Retorna id de persona</response>
        /// <response code="500">Error interno del servidor</response>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody]PersonaDto request)
        {
            try
            {
                Persona persona = _mapper.Map<PersonaDto, Persona>(request);

                var result = _personaService.CrearPersona(_personaRepository, persona);              
                return Created(new Uri($"{Request.Path}/{result.Id}", UriKind.Relative), null);
            }
            catch (CenturyException ex)
            {
                ObjectResult o = new ObjectResult(new ApiResponse(ex.CenturyError, ex.StackTrace));
                o.StatusCode = 500;
                return o;
            }
        }

        /// <summary>
        /// Modificar persona.
        /// </summary>
        /// <param name="request">PersonaDto Object.</param>
        /// <returns>Restorna ok</returns>
        /// <response code="201">Retorna ok</response>
        /// <response code="500">Error interno del servidor</response>
        [HttpPut()]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public IActionResult Put([FromBody]PersonaDto request)
        {
            try
            {
                Persona persona = _mapper.Map<PersonaDto, Persona>(request);

                var result = _personaService.ModificarPersonaById(_personaRepository, persona);
                return Ok();
            }
            catch (CenturyException ex)
            {                
                ObjectResult o = new ObjectResult(new ApiResponse(ex.CenturyError, ex.StackTrace));
                o.StatusCode = 500;
                return o;
            }
        }

        /// <summary>
        /// Crear Domicilio a una persona.
        /// </summary>
        /// <param name="request">DomicilioDto Object.</param>
        /// <param name="idPersona">Guid idPersona.</param>
        /// <returns>Restorna id de domicilio</returns>
        /// <response code="201">Retorna id de domicilio</response>
        /// <response code="404">No se encuentra la persona</response>
        /// <response code="500">Error interno del servidor</response>
        [HttpPost]
        [Route("{idPersona}/domicilios")]
        [ProducesResponseType(201)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult NewDomicilio(DomicilioDto request, Guid idPersona)
        {
            try
            {
                Domicilio domicilio = _mapper.Map<DomicilioDto, Domicilio>(request);

                var result = _personaService.CrearDomicilioPersona(_personaRepository, domicilio, idPersona);
                return Created(new Uri($"{Request.Path}/{result.Id}", UriKind.Relative), null);
            }
            catch (CenturyException ex)
            {
                ObjectResult o = new ObjectResult(new ApiResponse(ex.CenturyError, ex.StackTrace));
                o.StatusCode = 500;
                return o;
            }
        }

        /// <summary>
        /// Modificar Domicilio de una persona.
        /// </summary>
        /// <param name="request">DomicilioDto Object.</param>
        /// <param name="idPersona">Guid idPersona.</param>
        /// <param name="idDomicilio">Guid idDomicilio.</param>
        /// <returns>Restorna ok</returns>
        /// <response code="200">Retorna ok</response>
        /// <response code="404">No se encuentra la persona</response>
        /// <response code="500">Error interno del servidor</response>
        [HttpPut]
        [Route("{idPersona}/domicilios/{idDomicilio}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult EditDomicilio(DomicilioDto request, Guid idPersona, Guid idDomicilio)
        {
            try
            {
                Domicilio domicilio = _mapper.Map<DomicilioDto, Domicilio>(request);
                var result = _personaService.ModificarDomicilioPersona(_personaRepository, domicilio, idPersona, idDomicilio);
                return Ok();
            }
            catch (CenturyException ex)
            {
                ObjectResult o = new ObjectResult(new ApiResponse(ex.CenturyError, ex.StackTrace));
                o.StatusCode = 500;
                return o;
            }
        }
    }
}
