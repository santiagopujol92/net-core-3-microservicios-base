﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Application.ModelsDto;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Swagger
                services.AddMvc();
                services.AddSwaggerGen(c => {
                    c.SwaggerDoc("v1", new Info { Title = "Employee API", Version = "V1" });
                });
            // Fin Swagger
            // Automapper - Dependency Injection
            services.AddAutoMapper(typeof(Application.ModelsDto.AutoMapperMappingProfile));
            // Fin Automapper
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //Error Http localhost:44324/error/code
                app.UseStatusCodePagesWithReExecute("/error/{0}");
                app.UseExceptionHandler("/error/500");
                app.UseHttpsRedirection();
            //Fin Error Http

            //Swagger localhost:44324/swagger/index.html
            app.UseMvc();
                app.UseSwagger();
                app.UseSwaggerUI(c => {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "post API V1");
                });
            // Fin Swagger


        }

    }
}
